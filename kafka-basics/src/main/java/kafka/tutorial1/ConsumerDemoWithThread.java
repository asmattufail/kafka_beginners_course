package kafka.tutorial1;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class ConsumerDemoWithThread {
    private static Logger logger = LoggerFactory.getLogger(ConsumerDemoWithThread.class.getName());

    public ConsumerDemoWithThread(){

    }

    public static void main(String[] args) {
        String bootstrapServer = "127.0.0.1:9092";
        String groupId = "my-fifth-application";
        String topic = "first_topic";

        CountDownLatch latch = new CountDownLatch(1);

        logger.info("Creating the consumer thread");
        Runnable consumerRunnable = new ConsumerDemoWithThread().new ConsumerThread(bootstrapServer, groupId, topic, latch);
        Thread consumerThread = new Thread(consumerRunnable);
        consumerThread.start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            logger.info("Caught shutdown hook");
            ((ConsumerThread)consumerRunnable).shutdown();
        }));

        try {
            latch.await();
        } catch (InterruptedException e) {
            logger.error("Application got interrupted", e);
        }finally{
            logger.info("Application is closing");
        }

    }

    class ConsumerThread implements Runnable{
        private CountDownLatch latch;
        private KafkaConsumer<String, String> consumer;
        private String bootstrapServer;
        private String groupId;
        private String topic;


        public ConsumerThread(String bootstrapServer, String groupId, String topic, CountDownLatch latch){
            this.bootstrapServer = bootstrapServer;
            this.groupId = groupId;
            this.topic = topic;
            this.latch = latch;
            Properties properties = setProperties();
            //Create Consumer
            consumer = new KafkaConsumer<>(properties);

        }

        @Override
        public void run() {
            //subscribe consumer to our topic
            consumer.subscribe(Arrays.asList(topic));
            try {
                //poll for new data
                while (true) {
                    ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));

                    for (ConsumerRecord<String, String> record : records) {
                        logger.info("Key: " + record.key() + " Value: " + record.value());
                        logger.info("Partition: " + record.partition() + " , Offset: " + record.offset());
                    }
                }
            }catch (WakeupException e){
                logger.info("Received shutdown signal");
            }finally {
                consumer.close();
                latch.countDown();
            }
        }


        private Properties setProperties() {
            //Create Consumer Config
            Properties properties = new Properties();
            properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
            properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
            properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
            properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
            properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
            return properties;
        }

        public void shutdown(){
            consumer.wakeup();
        }
    }
}
